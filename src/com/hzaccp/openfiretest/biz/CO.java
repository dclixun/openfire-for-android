package com.hzaccp.openfiretest.biz;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.os.Handler;

import com.hzaccp.openfiretest.ChatActivity;
import com.hzaccp.openfiretest.UsersActivity;

/**
 * openfire操作工具类
 * */
public class CO {
	private static CO co;

	private CO() {

	}

	public static CO getInstance() {
		if (co == null) {
			co = new CO();
		}
		return co;
	}

	/**
	 * 信息监听
	 * */
	public void messListener() {
		new Thread(new Runnable() {
			public void run() {
				XMPPConnection con = SE.getInstance().getCon();
				con.getChatManager().addChatListener(new ChatManagerListener() {
					public void chatCreated(Chat chat, boolean createdLocally) {
						if (!createdLocally) {
							chat.addMessageListener(new MessageListener() {
								public void processMessage(Chat chat, Message mess) {
									Object[] cm = new Object[] { chat, mess };
									android.os.Message msg = handler.obtainMessage();
									msg.what = 1;//收到信息
									msg.obj = cm;
									msg.sendToTarget();
								}
							});
						}
					}
				});
			}
		}).run();
	}

	/**
	 * 文件监听
	 * */
	public void fileListener() {
		new Thread(new Runnable() {
			public void run() {
				XMPPConnection con = SE.getInstance().getCon();
				FileTransferManager fileTransferManager = new FileTransferManager(con);
				fileTransferManager.addFileTransferListener(new FileTransferListener() {
					@Override
					public void fileTransferRequest(FileTransferRequest fRequest) {
						android.os.Message msg = handler.obtainMessage();
						msg.what = 2;//收到文件请接收请求
						msg.obj = fRequest;
						msg.sendToTarget();
					}
				});
			}
		}).run();
	}

	/**
	 * 得到用户列表
	 * */
	public void getUserList() {
		new Thread(new Runnable() {
			public void run() {
				try {
					XMPPConnection con = SE.getInstance().getCon();
					Roster roster = con.getRoster();
					Collection<RosterGroup> rgs = roster.getGroups();

					ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
					for (RosterGroup rg : rgs) {
						Collection<RosterEntry> res = rg.getEntries();
						for (RosterEntry re : res) {
							HashMap<String, Object> map = new HashMap<String, Object>();
							map.put("userName", re.getName());
							map.put("userId", re.getUser());
							listItem.add(map);
						}
					}

					android.os.Message msg = handler.obtainMessage();
					msg.what = 3;//处理好友列表信息
					msg.obj = listItem;
					msg.sendToTarget();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).run();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			UsersActivity ua = (UsersActivity) SE.getInstance().getUserActivity();
			switch (msg.what) {
			case 1://收到聊天信息
				Object[] objs = (Object[]) msg.obj;
				exclMessage(objs);
				break;
			case 2://收到文件请接收请求
				FileTransferRequest fRequest = (FileTransferRequest) msg.obj;
				exclFile(fRequest);
				break;
			case 3://收到好友列表
				@SuppressWarnings("unchecked")
				ArrayList<HashMap<String, Object>> listItem = (ArrayList<HashMap<String, Object>>) msg.obj;
				if (ua != null) {
					ua.initUserList(listItem);
				}
				break;
			case 4://文件接收完成
				String[] strs = (String[]) msg.obj;
				exclFileSucces(strs);
				break;
			default:
				break;
			}

		};
	};

	/**
	 * 处理接收到的本地信息
	 * */
	private void exclMessage(Object[] objs) {
		Message m = (Message) objs[1];
		if (m.getType() == Type.chat) {//聊天信息
			String cu = SE.getInstance().getCu();//当前聊天界面的会话
			String from = m.getFrom().split("/")[0];
			if (cu != null && cu.equals(from)) {//找到会话
				ChatActivity ca = (ChatActivity) SE.getInstance().getChatActivity();
				if (ca != null) {
					ca.appMess(m.getBody(), 1);
				}
			} else {//没有会话
				UsersActivity ua = (UsersActivity) SE.getInstance().getUserActivity();
				if (ua != null) {
					ua.updateNewMess(from, 1);//修改未读数量
				}
				DB.getInstance().appMes(from, m.getBody());//保存在临时话会中
			}
		}
	}

	/**
	 * 接收到文件请求
	 * */
	private void exclFile(final FileTransferRequest request) {
		final IncomingFileTransfer infiletransfer = request.accept();
		AlertDialog.Builder builder = null;
		if (SE.getInstance().getChatActivity() != null) {
			builder = new AlertDialog.Builder(SE.getInstance().getChatActivity());
		} else {
			builder = new AlertDialog.Builder(SE.getInstance().getUserActivity());
		}
		builder.setTitle("接收文件").setCancelable(false).setPositiveButton("接收", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				try {
					final String path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/" + Math.random() + request.getFileName();
					File file = new File(path);
					if (!file.exists()) {
						file.createNewFile();
					}
					//request.getDescription();//发送文件时的第三个参数
					infiletransfer.recieveFile(file);//保存文件

					new Thread(new Runnable() {
						public void run() {
							while (true) {
								try {
									Thread.sleep(500L);
									if ((infiletransfer.getAmountWritten() >= request.getFileSize())
											|| (infiletransfer.getStatus() == FileTransfer.Status.error)
											|| (infiletransfer.getStatus() == FileTransfer.Status.refused)
											|| (infiletransfer.getStatus() == FileTransfer.Status.cancelled)
											|| (infiletransfer.getStatus() == FileTransfer.Status.complete)) {
										android.os.Message msg = handler.obtainMessage();
										msg.what = 4;//文件接收完成
										String[] objs = new String[] { path, infiletransfer.getPeer() };
										msg.obj = objs;
										msg.sendToTarget();
										break;
									}
								} catch (Exception err) {
									err.printStackTrace();
								}
							}
						}
					}).start();

					dialog.dismiss();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
		}).setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				request.reject();
				dialog.cancel();
			}
		}).show();
	}

	/**
	 * 文件传输完成
	 * */
	private void exclFileSucces(String[] strs) {
		String cu = SE.getInstance().getCu();//当前聊天界面的会话
		String from = strs[1].split("/")[0];
		if (cu != null && cu.equals(from)) {//找到会话
			ChatActivity ca = (ChatActivity) SE.getInstance().getChatActivity();
			if (ca != null) {
				ca.appMess(strs[0], 3);
			}
		} else {//没有会话
			UsersActivity ua = (UsersActivity) SE.getInstance().getUserActivity();
			if (ua != null) {
				ua.updateNewMess(from, 1);//修改未读数量
			}
			DB.getInstance().appMes(from, strs[0]);//保存在零时话会中
		}
	}
}
